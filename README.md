# Babel Plugin Transform Functional Return

This plugin adds support for "functional" return statements. Inspired by the `return if` functionality in ruby. It helps functional code look that much more functional.

## Installation

```javascript
$ npm i -D babel-plugin-transform-functional-return

// add to the .babelrc file (or in package.json babel entry):
{
    "plugins": [
        "transform-functional-return",
    ]
}
```

## How To Use

```javascript   
// three lines! so dirty...
if (truthy) {
    return;
}

// "goto fail" bug so don't:
if (truthy) return;

// *click tongue* nice
ReturnIf(truthy);

// compiles to:
// if (truthy) {
//     return;
// }
```

You can supply a return value:

```javascript
ReturnIf(truthy, true);

// compiles to:
// if (truthy) {
//     return true;
// }
```

If you want to call a function, then call it. Don't put the function itself as a "parameter":

```javascript
ReturnIf(truthy, doThisIfReturningEarly());

// compiles to:
// if (truthy) {
//     return doThisIfReturningEarly();
// }

ReturnIf(truthy, await doThisIfReturningEarly());

// compiles to:
// if (truthy) {
//     return await doThisIfReturningEarly();
// }

// don't do this (unless your intent is to return the function as a value):
ReturnIf(truthy, doThisIfReturningEarly);

// compiles to:
// if (truthy) {
//     return doThisIfReturningEarly;
// }
```

`ContinueIf` and `BreakIf` exist as well. They function as one would expect. You can't specify a return value for those.

```javascript
let counter = 0;
while (counter) {
    ContinueIf(counter < 10);
    BreakIf(counter > 10);

    console.log(counter);

    counter += 1;
}
```

`ReturnIf`, `ContinueIf`, and `BreakIf` have placeholders to put into your uncomplied code. This keeps linters and typescript happy. Treeshaking will remove the import statement so it doesn't bloat your compiled code since the functions aren't actually used in the end.

```javascript
import { ReturnIf } from 'babel-plugin-transform-functional-return';

export default SomeModule(reasonToReturnEarly) {
    ReturnIf(reasonToReturnEarly, false);

    // stuff
}
```

# ThrowIf

```javascript
function doSomething(someValue) {
    ThrowIf(someValue === undefined, new Error("someValue is undefined!"));
    // ...
}

// compiles to:
function doSomething(someValue) {
    if (someValue === undefined) {
        throw new Error("someValue is undefined!");
    }
    // ...
}

// compare to:
function doSomething(someValue) {
    throwIf(someValue === undefined, "someValue is undefined!");
    // ...
}

function throwIf(truthy, message) {
    if (truthy) {
        throw new Error(message);
    }
}
```

`ThrowIf` has some benefits over `throwIf`. Mainly, the second parameter doesn't get executed if `truthy` is false. Whereas, if you call a function or make a complicated string. All that business is executed even if you don't have an error to throw. Next, the error comes from the function causing the error, not throwIf. In other words: cleaner stack track.


## Why not `return unless` too?

In ruby `return if x` returns early if `x` is true. `Return unless x` will return early unless `x` is true.

I'm not against adding `unless` variants; it wouldn't be difficult to add. But when you can get `return unless` by adding `!` (e.g., `ReturnIf(!something)`) it doesn't make sense (to me) adding more higher-level choices.

## Contact

You can send me an email at [steven@mrogers.email](mailto:steven@mrogers.email).

module.exports = function ReturnIfPlugin({ types: t }) {
	return {
		visitor: {
			CallExpression(path) {
				switch (path.node.callee.name) {
					case 'ReturnIf':
						path.parentPath.replaceWith(
							t.ifStatement(
								path.node.arguments[0],
								t.blockStatement([t.returnStatement(path.node.arguments[1])]),
							),
						);
						break;

					case 'ContinueIf':
						path.parentPath.replaceWith(
							t.ifStatement(
								path.node.arguments[0],
								t.blockStatement([t.continueStatement()]),
							),
						);
						break;

					case 'BreakIf':
						path.parentPath.replaceWith(
							t.ifStatement(
								path.node.arguments[0],
								t.blockStatement([t.breakStatement()]),
							),
						);
						break;

					case "DoIf": 
						path.parentPath.replaceWith(
							t.ifStatement(
								path.node.arguments[0],
								t.blockStatement([t.expressionStatement(path.node.arguments[1])]),
							)
						);
						break;

					case "ThrowIf": 
						path.parentPath.replaceWith(
							t.ifStatement(
								path.node.arguments[0],
								t.blockStatement([
									t.throwStatement(path.node.arguments[1])
								]),
							),
						);
						break;
				}
			},
		},
	};
};

// -----------------------------------------------------------------------------


module.exports.ReturnIf = function ReturnIf(truthy, returnValue) {
	throw new Error("ReturnIf wasn't compiled");
}

module.exports.ContinueIf = function ContinueIf(truthy, returnValue) {
	throw new Error("ContinueIf wasn't compiled");
}

module.exports.BreakIf = function BreakIf(truthy, returnValue) {
	throw new Error("BreakIf wasn't compiled");
}

module.exports.DoIf = function DoIf(truthy, returnValue) {
	throw new Error("DoIf wasn't compiled");
}

module.exports.ThrowIf = function ThrowIf(truthy, error) {
	throw new Error("ThrowIf wasn't compiled");
}

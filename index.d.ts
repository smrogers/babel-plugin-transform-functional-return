export function ReturnIf(truthy: any, returnValue?: any): void;
export function ContinueIf(truthy: any, returnValue?: any): void;
export function BreakIf(truthy: any, returnValue?: any): void;
export function DoIf(truthy: any, doValue?: any): void;
export function ThrowIf(truthy: any, error: Error): void;

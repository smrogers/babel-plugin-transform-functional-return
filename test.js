const fs = require('node:fs');
const path = require('node:path');
const babel = require('@babel/core');

const ReturnIfPlugin = require('./index');

// -----------------------------------------------------------------------------

const code = `
ReturnIf(false, false);
BreakIf(false);
ContinueIf(true);
`;

// -----------------------------------------------------------------------------

const output = babel.transformSync(code, {
	plugins: [ReturnIfPlugin],
});
console.log(output.code);

// -----------------------------------------------------------------------------

fs.writeFileSync(
	path.join(__dirname, 'output.json'),
	JSON.stringify(
		babel.parseSync(code, {
			plugins: [],
		}),
		null,
		'\t',
	),
);
